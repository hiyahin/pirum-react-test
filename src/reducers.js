import { combineReducers } from 'redux';
import albums from './components/App/reducers.js';

const albumApp = combineReducers({
  albums
})

export default albumApp