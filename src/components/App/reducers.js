let newState = [];
const actions = (state = [], action) => {
  switch (action.type) {
    case 'LOAD_ALBUMS':
      return action.albums
    case 'TOGGLE_ALBUM':
      newState =  state.slice(0);
      newState[action.bandIndex].albums[action.albumIndex].open = !state[action.bandIndex].albums[action.albumIndex].open;
      return newState;
    case 'TOGGLE_ALL_ALBUMS':
      newState =  state.map((band) => {
        return {
          band: band.band,
          albums: band.albums.map((album) => {
            return {
              name: album.name,
              songs: album.songs,
              open: action.open
            }
          })
        }
      })
      return newState;
    default:
      return state
  }
}

export default actions