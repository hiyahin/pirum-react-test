import fetch from 'isomorphic-fetch'

export const loadAlbums = () => {
	return dispatch => {
		fetch('data.json')
		.then(response => response.json())
		.then(data => dispatch(storeAlbums(data)));
	}
}

export const storeAlbums = (albums) => {
	//Restrucure the data from the JSON to a more structured array
	let structuredAlbums = [];
	albums.forEach(function (item){
		let bandIndex = structuredAlbums.findIndex((album) => album.band == item.band);

		if (bandIndex < 0) {
			structuredAlbums.push({
				band: item.band,
				albums:[]
			})
			bandIndex = structuredAlbums.length - 1;
		}


		let albums = structuredAlbums[bandIndex].albums;
		let albumIndex = albums.findIndex((album) => album.name == item.album);

		if (albumIndex < 0) {
			albums.push({
				name: item.album,
				open: false,
				songs:[]
			})
			albumIndex = albums.length - 1;
		}

		//Allow duplicate song names in same album
		albums[albumIndex].songs.push(item.song)
	});

	structuredAlbums.sort((a, b) => {
		return a.band > b.band
	})

	return {
		type: 'LOAD_ALBUMS',
		albums: structuredAlbums
	}
}

export const toggleAlbum = (bandIndex, albumIndex) => {
	return {
		type: 'TOGGLE_ALBUM',
		bandIndex: bandIndex,
		albumIndex: albumIndex
	}
}

export const toggleAllAlbums = (open) => {
	return {
		type: 'TOGGLE_ALL_ALBUMS',
		open: open,
	}
}