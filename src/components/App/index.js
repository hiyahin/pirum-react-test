import React from 'react';
import AlbumList from '../AlbumList';
import { loadAlbums, toggleAlbum, toggleAllAlbums } from './actions.js';
import { connect } from 'react-redux';

class App extends React.Component {
	constructor(props, context) {
    super(props, context);
		this.state = {
			globalToggle: true
		}
	}

	onGlobalToggleClick(open) {
		this.setState({globalToggle: !this.state.globalToggle});
		this.props.toggleAllAlbums(open);
	}

	componentDidMount() {
		this.props.loadData();
	}

  render() {
    return (
			<div>
				<h1 onClick={this.onGlobalToggleClick.bind(this, this.state.globalToggle)}>Albums <span className={"album_toggle " + (this.state.globalToggle ? 'open' : 'close')}></span></h1>
				<AlbumList albums={this.props.albums} onAlbumClick={this.props.onAlbumClick} />
			</div>
    )
  }
}

const mapStateToProps = state => {
	return {
		albums: state.albums
	}
}

const mapDispatchToProps = dispatch => {
	return {
		toggleAllAlbums: (open) => {
			dispatch(toggleAllAlbums(open))
		},
		onAlbumClick: (bandIndex, albumIndex) => {
			dispatch(toggleAlbum(bandIndex, albumIndex))
		},
		loadData: () => {
			dispatch(loadAlbums())
		}
	}
}

const connectedApp = connect (
	mapStateToProps,
	mapDispatchToProps
)(App)

export default connectedApp