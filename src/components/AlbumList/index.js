import React from 'react';

import SongList from '../SongList';

const AlbumList = (props) => {
	let albumItems = props.albums.map((band, bandIndex) => {
		return band.albums.map((bandAlbum, albumIndex) => {
			let albumSongList = bandAlbum.open ? <SongList songs={bandAlbum.songs} /> : null
			return (<li>
				<span onClick={props.onAlbumClick.bind(null, bandIndex,albumIndex)}>{band.band} - {bandAlbum.name} <span className={"album_toggle " + (bandAlbum.open ? 'close' : 'open')}></span></span>
				{albumSongList}
			</li>
			)
		});
	});
  return (
		<ul>
			{albumItems}
		</ul>
  );
}

export default AlbumList