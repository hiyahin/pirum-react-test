import React from 'react';

const songList = (props) => {
		let songItems = props.songs.map((song) => {
			return <li key={song}>{song}</li>
		});
  return (
		<ul>
			{songItems}
		</ul>
  );
}

export default songList